# Getting Started with HTML Principles

Getting started in UI development can be daunting and confusing.  In this video, we try to de-mystify getting started as a UI developer and how to start analyzing different HTML principles.

**The video for this code tutorial can be found here:**

https://www.youtube.com/watch?v=Fr9oLrh_sc0&feature=youtu.be